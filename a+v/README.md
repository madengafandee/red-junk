This is a sample Red script which purpose is to *automatically hook an external audio track for any video file played*.
It works transparently by finding the appropriate audio and invoking your favorite media player, presenting you with a choice when appropriate.

Apart from utility value it tests `glob` and abuses `composite` ☻

### Binaries for Windows
[Are here](https://gitlab.com/hiiamboris/red-junk/tree/release/a+v)

### Usage for Windows noobs
- Edit `a+v.conf` to set your command lines
- RTFM and use `assoc` and `ftype` commands to invoke the `a+v.exe` instead, like this:
```
for %i in (.mkv .mp4 .wmv) do assoc %i=player
ftype player=c:\bin\a+v.exe "%1"
```

### Compilation
[Red nightly build](https://www.red-lang.org/p/download.html) -- [`glob`](https://gitlab.com/hiiamboris/red-junk/raw/master/glob/glob.red) -- [Gregg Irwin's `composite`](https://gist.github.com/hiiamboris/d4716ade2171a5c4c5ed9f96fbfd5446/raw/2b8543edbba3eb95ec3354258b8819edbf4bf8ef/composite.red)

`$ red -r a+v.red`

### Preview
<img src="https://i.gyazo.com/5ab3f989c9941a6291b614185d6d880b.png" alt="screenshot" width=400 height=300>
